package com.foxxyy.why.wxhookdemo.manager;

import com.foxxyy.why.wxhookdemo.constant.MessageCValue;
import com.foxxyy.why.wxhookdemo.model.Message;
import com.foxxyy.why.wxhookdemo.opt.WeChatOpt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @Author foxxyy
 * @Date 2018-10-15
 */
public class ManualOptManger {
    /**
     * 名单内username发的消息有效。
     */
    private static final List<String> USERNAME_WHITE_LIST = new ArrayList<String>() {
        {
            add("wxid");
        }
    };
    private static final HashMap<String, Act> COMMAND_MAP = new LinkedHashMap<String, Act>() {
        {
            put("cmd", new Act() {
                public void doWork(String param, String talker) {
                    // TODO: excute method
                    WeChatOpt.getInstance().send(talker, param);
                }
            });
        }
    };

    /**
     * 是否需要机器人对输入内容作出对应动作。用于实现机器人设置。
     */
    public static boolean systemInput(Message stuff) {
        if (MessageCValue.TYPE__TEXT != stuff.getMsgType()
                || stuff.getStatus() == null) {
            return false;
        }

        String talker = stuff.getTalker();
        String rawContent = stuff.getContent();

        if (stuff.isReceivedMsg()) {
            String fromUsername;
            if (talker.endsWith("@chatroom")) {
                int idx = rawContent.indexOf(":\n");
                if (idx == -1) {
                    return false;
                }
                fromUsername = rawContent.substring(0, idx);
                rawContent = rawContent.substring(idx + 2);
            } else {
                fromUsername = talker;
            }
            if (!USERNAME_WHITE_LIST.contains(fromUsername)) { //仅处理白名单内username发送的消息
                return false;
            }
        }
        String command;
        String param = null;
        int idx = rawContent.indexOf("::"); //分隔命令及参数
        if (idx == -1) {
            command = rawContent;
        } else {
            command = rawContent.substring(0, idx);
            param = rawContent.substring(idx + 1);
        }
        Act acotr = COMMAND_MAP.get(command.trim().toLowerCase());
        try {
            if (acotr != null) {
                acotr.doWork(param, talker);
                return true;
            }
        } catch (Throwable throwable) {
            // TODO: log
        }
        return false;
    }

    private interface Act {
        void doWork(String param, String talker);
    }

}

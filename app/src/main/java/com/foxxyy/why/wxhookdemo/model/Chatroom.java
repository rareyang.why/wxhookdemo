package com.foxxyy.why.wxhookdemo.model;

import android.content.ContentValues;
import com.foxxyy.why.wxhookdemo.constant.ChatroomColumn;

import java.util.List;

/**
 * @Author foxxyy
 * @Date 2018-10-18
 */
public class Chatroom {
    private Integer rowid;
    private String chatroomName; //群username
    private String memberList; //成员username，“;”分隔
    private String displayName; // 成员昵称nickname, “、”分隔
    private String roomowner; // 群主username
    private String selfDisplayName; // 机器人群昵称
    private String chatroomNotice; // 群公告
    private String chatroomNoticeEditor; // 群公告编辑人username
    private Long chatroomNoticePublishTime; // 群公告发布时间
    private String chatroomNickname; // 群名称
    private List<String> usernames;

    public Chatroom() {
    }

    public Chatroom(ContentValues contentValues) {
        this.rowid = contentValues.getAsInteger(ChatroomColumn.ROW_ID); //群username
        this.chatroomName = contentValues.getAsString(ChatroomColumn.CHATROOMNAME); //群username
        this.memberList = contentValues.getAsString(ChatroomColumn.MEMBER_LIST); //成员username，“;”分隔
        this.displayName = contentValues.getAsString(ChatroomColumn.DISPLAYNAME); // 成员昵称nickname, “、”分隔
        this.roomowner = contentValues.getAsString(ChatroomColumn.ROOM_OWNER); // 群主username
        this.selfDisplayName = contentValues.getAsString(ChatroomColumn.SELF_DISPLAY_NAME); // 机器人群昵称
        this.chatroomNotice = contentValues.getAsString(ChatroomColumn.CHATROOM_NOTICE); // 群公告
    }

    public Integer getRowid() {
        return rowid;
    }

    public void setRowid(Integer rowid) {
        this.rowid = rowid;
    }

    public String geChatroomColumnName() {
        return chatroomName;
    }

    public void seChatroomColumnName(String chatroomName) {
        this.chatroomName = chatroomName;
    }

    public String getMemberList() {
        return memberList;
    }

    public void setMemberList(String memberList) {
        this.memberList = memberList;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getRoomowner() {
        return roomowner;
    }

    public void setRoomowner(String roomowner) {
        this.roomowner = roomowner;
    }

    public String getSelfDisplayName() {
        return selfDisplayName;
    }

    public void setSelfDisplayName(String selfDisplayName) {
        this.selfDisplayName = selfDisplayName;
    }

    public String geChatroomColumnNotice() {
        return chatroomNotice;
    }

    public void seChatroomColumnNotice(String chatroomNotice) {
        this.chatroomNotice = chatroomNotice;
    }

    public String geChatroomColumnNoticeEditor() {
        return chatroomNoticeEditor;
    }

    public void seChatroomColumnNoticeEditor(String chatroomNoticeEditor) {
        this.chatroomNoticeEditor = chatroomNoticeEditor;
    }

    public Long geChatroomColumnNoticePublishTime() {
        return chatroomNoticePublishTime;
    }

    public void seChatroomColumnNoticePublishTime(Long chatroomNoticePublishTime) {
        this.chatroomNoticePublishTime = chatroomNoticePublishTime;
    }

    public String geChatroomColumnNickname() {
        return chatroomNickname;
    }

    public void seChatroomColumnNickname(String chatroomNickname) {
        this.chatroomNickname = chatroomNickname;
    }

    public List<String> getUsernames() {
        return usernames;
    }

    public void setUsernames(List<String> usernames) {
        this.usernames = usernames;
    }
}

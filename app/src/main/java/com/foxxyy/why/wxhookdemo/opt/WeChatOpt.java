package com.foxxyy.why.wxhookdemo.opt;

import android.content.Intent;
import android.graphics.Bitmap;
import com.foxxyy.why.wxhookdemo.constant.ShSymbol;
import com.foxxyy.why.wxhookdemo.hooker.QrCodeeHooker;
import com.foxxyy.why.wxhookdemo.manager.RobotManager;
import com.foxxyy.why.wxhookdemo.util.ClassUtils;
import com.foxxyy.why.wxhookdemo.util.XxLog;
import com.foxxyy.why.wxhookdemo.util.StringUtils;
import de.robv.android.xposed.XposedHelpers;

import java.util.*;

import static java.lang.Thread.sleep;

/**
 * @Author foxxyy
 * @Date 2018-09-30
 */
public class WeChatOpt {
    private WeChatOpt() {
    }

    private static WeChatOpt instance = new WeChatOpt();

    public static WeChatOpt getInstance() {
        return instance;
    }

    /**
     * 添加用户入群（群人数 < 40)
     *
     * @param chatroom  群ID
     * @param usernames 用户IDs
     */
    public void addChatMember(final String chatroom, final List<String> usernames) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                post(XposedHelpers.newInstance(
                        ClassUtils.mmClass(".chatroom.c.e"),
                        chatroom, usernames, null));
            }
        };
        RobotManager.weChatMainHandler.post(runnable);
    }

    public Bitmap getAvatar(String username) {
        return (Bitmap) XposedHelpers.callMethod(
                XposedHelpers.callStaticMethod(ClassUtils.mmClass(".ae.q"), "JC"),
                "b", username, false, -1);
    }

    /**
     * 踢出群成员
     *
     * @param chatroom    成员所在群
     * @param toUsernames 要踢出的成员ID
     */
    public void kickOutMember(final String chatroom, final List<String> toUsernames) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                post(XposedHelpers.newInstance(
                        ClassUtils.mmClass(".chatroom.c.h")
                        , chatroom, toUsernames, 0));
            }
        };
        RobotManager.weChatMainHandler.post(runnable);
    }

    /**
     * 修改群名称
     * 见微信内: ModRemarkRoomNameUI  关键字：【(27, 】
     *
     * @param chatroom 群ID
     * @param name     新的群名称
     */
    public void modChatroomName(String chatroom, String name) {
        Object obj0_0 = XposedHelpers.callMethod(
                XposedHelpers.newInstance(ClassUtils.mmClass(".protocal.c.bks")),
                "Xp", chatroom);
        Object obj0_1 = XposedHelpers.callMethod(
                XposedHelpers.newInstance(ClassUtils.mmClass(".protocal.c.bks")),
                "Xp", name);

        Object obj1_0 = XposedHelpers.newInstance(ClassUtils.mmClass(".protocal.c.avx"));
        XposedHelpers.setObjectField(obj1_0, "rMh", obj0_0);
        XposedHelpers.setObjectField(obj1_0, "sIW", obj0_1);

        Object obj2_0 = XposedHelpers.newInstance(ClassUtils.mmClass(".plugin.messenger.foundation.a.a.i.a"), 27,
                obj1_0);

        someGoGo();
        XposedHelpers.callMethod(
                XposedHelpers.callStaticMethod(ClassUtils.mmClass(".model.c"), "EN"),
                "b", obj2_0);
    }


    /**
     * 修改群公告
     *
     * @param chatroom 群ID
     * @param notice   新的群公告内容
     */
    public void alterRoomNotice(final String chatroom, final String notice) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                post(XposedHelpers.newInstance(
                        ClassUtils.mmClass(".chatroom.c.o"),
                        chatroom, notice));
            }
        };
        RobotManager.weChatMainHandler.post(runnable);
    }


    /**
     * 打开二维码页面
     *
     * @param username 群ID 为空代表打开当前微信的二维码
     */
    public void openSelfQRCodeUI(String username, String OSSKey) {
        final Intent intent = new Intent().putExtra(ShSymbol.SOHU_ACT, "sh_robot_qrcode");
        if (StringUtils.isNotEmpty(username) && username.endsWith("@chatroom")) {
            intent.putExtra("from_userName", username);
        }
        if (StringUtils.isNotEmpty(OSSKey)) {
            intent.putExtra("bjOSSKey", OSSKey);
        }
        QrCodeeHooker.getInstance().hook();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                XposedHelpers.callStaticMethod(
                        ClassUtils.mmClass(".bm.d"), "b",
                        RobotManager.appContext, "setting", ".ui.setting.SelfQRCodeUI",
                        intent
                );
            }
        };
        RobotManager.weChatMainHandler.post(runnable);
    }

    public void creatChatroom(List<String> usernames, String topic) {
        if (usernames == null || usernames.isEmpty()) {
            return;
        }
        List<String> roomMemberlist = new ArrayList<>(usernames);
        if (roomMemberlist.size() == 1) {
            return;
        }
        if (topic == null) {
            topic = "";
        }
        createChatroomWithTopic(usernames, topic);
    }

    /**
     * 新建群
     *
     * @param usernames 群成员
     * @param topic     群名称
     */
    public void createChatroomWithTopic(final List<String> usernames, final String topic) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                Object createChatroomObj = XposedHelpers.newInstance(ClassUtils.mmClass(".chatroom.c.g"), topic, usernames);
                post(createChatroomObj);
            }
        };
        RobotManager.weChatMainHandler.post(runnable);
    }

    public void send(final String talker, final String content) {
        if (StringUtils.isBlank(talker) || StringUtils.isBlank(content)) {
            XxLog.info("Blank talker or content.");
            return;
        }
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                Object obj = XposedHelpers.newInstance(
                        ClassUtils.mmClass(".modelmulti.h"),
                        talker, content, 1, 0, null
                );
                post(obj);
            }
        };
        RobotManager.weChatMainHandler.post(runnable);
    }

    private static final String AT_TAG = String.valueOf((char) 8197);

    public void sendAtInRoom(final String talker, final String content, final List<String> toUsernames) {
        if (StringUtils.isBlank(talker) || StringUtils.isBlank(content) || !talker.endsWith("@chatroom")
                || !content.contains("@") || !content.contains(AT_TAG)) {
            XxLog.info("Blank talker or content or wrong talker.");
            return;
        }
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                Object obj = XposedHelpers.newInstance(
                        ClassUtils.mmClass(".modelmulti.h"),
                        talker, content, 1, 291, atSomeone(toUsernames)
                );
                post(obj);
            }
        };
        RobotManager.weChatMainHandler.post(runnable);
    }

    private HashMap<String, String> atSomeone(List<String> usernames) {
        StringBuilder stringBuilder = new StringBuilder();
        for (String username : usernames) {
            stringBuilder.append(username).append(",");
        }
        stringBuilder.delete(stringBuilder.length() - 1, stringBuilder.length());
        String value = "<![CDATA[" + stringBuilder + "]]>";
        HashMap<String, String> map = new HashMap<>();
        map.put("atuserlist", value);
        return map;
    }

    private void sendImage(final String filePath, final String talker) {
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                Object obj = XposedHelpers.newInstance(
                        ClassUtils.mmClass(".ap.l"),
                        4, RobotManager.username(),
                        talker, filePath, 0, null, 0, null, "", true, 2130970303, 0, -1000.0f, -1000.0f
                );
                post(obj);
            }
        };
        RobotManager.weChatMainHandler.post(runnable);
    }

    /**
     * 获取用户在群内的昵称
     */
    public String getNicknameInRoom(String chatroom, String username) {
        Object chatroomObj = getChatroomObj(chatroom);
        CharSequence roomNickname = (CharSequence) XposedHelpers.callStaticMethod(
                ClassUtils.mmClass(".ui.chatting.AtSomeoneUI"), "a", chatroomObj, username);
        return roomNickname == null ? "" : roomNickname.toString();
    }

    protected Object getChatroomObj(String chatroom) {
        return XposedHelpers.callMethod(
                XposedHelpers.callMethod(
                        XposedHelpers.callStaticMethod(ClassUtils.mmClass(".model.av"), "GP"),
                        "EX"
                ),
                "ii", chatroom
        );
    }

    public void acceptFriend(String username) {
        addFriend(true, username, "", "", 14);
    }

    public void addFriend(final boolean isAcceptOrOfficalAdd, final String stranger1, final String stranger2, final String comment, final int scene) {
        final Runnable runnable = new Runnable() {

            @Override
            public void run() {
                if (isAcceptOrOfficalAdd) {
                    if (StringUtils.isBlank(stranger1)) {
                        return;
                    }
                    XxLog.info("add friend Start " + stranger1);
                    int step;
                    List<String> list1 = Collections.singletonList(stranger1);
                    LinkedList<Integer> list2 = new LinkedList<>();
                    List<String> list3;
                    Map map;
                    step = 1;
                    list2.add(scene);
                    list3 = Collections.singletonList(genAntiSpamTicket(stranger1));
                    map = null;
                    // generate add friend object
                    Object obj = XposedHelpers.newInstance(
                            ClassUtils.mmClass(".pluginsdk.model.m"),
                            step, list1, list2, list3, comment, "", map, "", "");
                    post(obj);
                } else {
                    if (StringUtils.isBlank(stranger1) || StringUtils.isBlank(stranger2)) {
                        return;
                    }
                    int step = 1;
                    LinkedList<String> list1 = new LinkedList();
                    list1.add(stranger1);
                    LinkedList<Integer> list2 = new LinkedList();
                    list2.add(scene);
                    LinkedList<String> list3 = new LinkedList();
                    list3.add(stranger2);
                    Object obj1 = XposedHelpers.newInstance(
                            ClassUtils.mmClass(".pluginsdk.model.m"),
                            step, list1, list2, list3, "", "", null, "", "");
                    post(obj1);
                    try {
                        sleep(3 * 1000);// sleep 3再发
                    } catch (InterruptedException ignored) {
                    }
                    step = 2;
                    HashMap<String, Integer> map = new HashMap<>();
                    map.put(stranger1, 0);
                    Object obj2 = XposedHelpers.newInstance(
                            ClassUtils.mmClass(".pluginsdk.model.m"),
                            step, list1, list2, null, comment, "", map, null, "");
                    post(obj2);
                }
            }
        };

        RobotManager.weChatMainHandler.post(runnable);
    }

    private String genAntiSpamTicket(String username) {
        String result = (String) XposedHelpers.callMethod(
                XposedHelpers.callMethod(
                        XposedHelpers.callStaticMethod(ClassUtils.mmClass(".plugin.c.a"), "Ym"),
                        "Ft"
                ),
                "Zk", username
        );
        return result == null ? "" : result;
    }

    /**
     * 微信内基础post请求接口
     *
     * @param obj 请求实体
     */
    private void post(Object obj) {
        XposedHelpers.callMethod(
                XposedHelpers.callStaticMethod(ClassUtils.mmClass(".model.av"), "CB"),
                "d", obj
        );
    }

    private Object someGoGo() {
        return XposedHelpers.callStaticMethod(ClassUtils.mmClass(".model.av"), "GP");
    }
}

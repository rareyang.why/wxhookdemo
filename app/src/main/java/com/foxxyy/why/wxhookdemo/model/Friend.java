package com.foxxyy.why.wxhookdemo.model;

/**
 * @Author foxxyy
 * @Date 2018-10-12
 */
public class Friend {
    private String username; // 微信id
    private String alias; // 用户设置的微信号
    private String nickname; // 昵称
    private String conRemark; // 备注
    private String city; // 市
    private String province; // 省

    public Friend(String username, String alias, String nickname, String conRemark) {
        this.username = username;
        this.alias = alias;
        this.nickname = nickname;
        this.conRemark = conRemark;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getConRemark() {
        return conRemark;
    }

    public void setConRemark(String conRemark) {
        this.conRemark = conRemark;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }
}

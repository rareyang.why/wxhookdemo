package com.foxxyy.why.wxhookdemo.main;

import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import com.foxxyy.why.wxhookdemo.hooker.DBInsertHooker;
import com.foxxyy.why.wxhookdemo.manager.RobotManager;
import com.foxxyy.why.wxhookdemo.opt.WeChatDB;
import com.foxxyy.why.wxhookdemo.util.ClassUtils;
import com.foxxyy.why.wxhookdemo.util.XxLog;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedHelpers;

import java.sql.Time;

/**
 * @Author foxxyy
 * @Date 2018-09-30
 */
public class HookHome {
    private static HookHome hookHome = new HookHome();

    private HookHome() {
    }

    public static HookHome genInstance() {
        return hookHome;
    }

    private ClassLoader classLoader;

    public void hook() {
        try {
            classLoader = RobotManager.loadPackageParam.classLoader;
            hookAppContext();
            hookDataBase();
            Thread.sleep(1_000L); //
            hookDBInsert();
            //--------
            HookTester.hookTester();
        } catch (Throwable e) {
            XxLog.error("HookHome", e);
        }
    }

    /**
     * hook微信上下文
     */
    private void hookAppContext() {
        Class applicationLikeClass = ClassUtils.mmClass(".app.MMApplicationLike");
        String methodName = "onCreate";
        XposedHelpers.findAndHookMethod(
                applicationLikeClass,
                methodName,
                new XC_MethodHook() {
                    @Override
                    protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                        Application application =
                                (Application) XposedHelpers.callMethod(param.thisObject, "getApplication");
                        Context context = application.getApplicationContext();
                        RobotManager.setAppContext(context);
                    }
                });
    }

    /**
     * hook微信DB
     */
    private void hookDataBase() {
        Class clazz = ClassUtils.mmClass(".storage.z");
        Class clazz2 = ClassUtils.mmClass(".bz.h");
        XposedHelpers.findAndHookConstructor(clazz, clazz2, new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                WeChatDB weChatDb = new WeChatDB(param.args[0]);
                RobotManager.setWeChatDb(weChatDb);
                RobotManager.setSelfWeChat(weChatDb.querySelfWeChatInfo());
            }
        });
    }


    /**
     * hook微信DB insert操作
     */
    private void hookDBInsert() {
        Class weChatDatabase = XposedHelpers.findClass("com.tencent.wcdb.database.SQLiteDatabase", classLoader);
        String methodInsert = "insertWithOnConflict";
        XposedHelpers.findAndHookMethod(
                weChatDatabase, methodInsert,
                String.class, String.class, ContentValues.class, Integer.TYPE,
                new DBInsertHooker());
    }


}

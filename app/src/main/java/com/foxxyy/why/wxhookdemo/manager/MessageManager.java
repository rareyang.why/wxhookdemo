package com.foxxyy.why.wxhookdemo.manager;

import com.foxxyy.why.wxhookdemo.constant.MessageCValue;
import com.foxxyy.why.wxhookdemo.model.Message;
import com.foxxyy.why.wxhookdemo.opt.WeChatOpt;
import com.foxxyy.why.wxhookdemo.util.XxLog;
import com.google.gson.Gson;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.regex.Pattern;

/**
 * @Author foxxyy
 * @Date 2019-03-05
 */
public class MessageManager {
    private static final Gson gson = new Gson();
    private static final Pattern AT_PATTERN = Pattern.compile("@.*?" + String.valueOf((char) 8197));

    public static void processMessagePrismRecord(Message stuff) throws IOException, XmlPullParserException {
        if (stuff == null) {
            return;
        }
        XxLog.info("Message stuff: " + gson.toJson(stuff));

        Integer msgType = stuff.getMsgType();
        String content = stuff.getContent();
        String talker = stuff.getTalker();
        if (MessageCValue.TYPE__SYS == msgType || MessageCValue.TYPE__SYS_B == msgType || MessageCValue.TYPE__ROOM_HINT == msgType) {
            if (content.endsWith("刚刚把你添加到通讯录，现在可以开始聊天了。")
                    || content.endsWith("剛剛把你新增到通訊錄，現在可以開始聊天了。")) {
                dealAddFriend(talker);
            }
            if ((content.startsWith("你已添加") && content.endsWith("现在可以开始聊天了。"))
                    || (content.startsWith("你已加") && content.endsWith("現在可以聊天了。"))) {
                dealAddFriend(talker);
            }
        }
    }

    public static void dealAddFriend(String username) {
        WeChatOpt.getInstance().send(username, "Hello world!"); //发送内容
    }
}

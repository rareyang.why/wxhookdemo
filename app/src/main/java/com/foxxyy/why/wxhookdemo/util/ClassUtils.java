package com.foxxyy.why.wxhookdemo.util;

import com.foxxyy.why.wxhookdemo.constant.WeChatCode;
import com.foxxyy.why.wxhookdemo.manager.RobotManager;
import de.robv.android.xposed.XposedHelpers;

/**
 * @Author foxxyy
 * @Date 2018-09-30
 */
public class ClassUtils {
    public static Class mmClass(String className) {
        return XposedHelpers.findClass(WeChatCode.PACKAGE_NAME + className, RobotManager.loadPackageParam.classLoader);
    }

}

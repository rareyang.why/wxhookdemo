package com.foxxyy.why.wxhookdemo.constant;

/**
 * @Author foxxyy
 * @Date 2018-10-15
 * @Desc Table message columns' value
 */
public class MessageCValue {
    public static final int STATUS__SEND = 1;
    public static final int STATUS__COMPLETE = 2;
    public static final int STATUS__RECEIVE = 3;
    public static final int STATUS__OPERATE = 4;
    public static final int STATUS__FAILED = 5;
    public static final int STATUS__FRIEDN_REQUEST = 6;

    public static final int IS_SEND__TRUE = 1;
    public static final int IS_SEND__FALSE = 0;

    public static final int TYPE__TEXT = 1; // 文本
    public static final int TYPE__IMAGE = 3; // 图片
    public static final int TYPE__VOICE = 34; // 语音
    public static final int TYPE__CONTACT_CARD = 42; // 名片
    public static final int TYPE__VIDEO = 43; // 视频
    public static final int TYPE__STICKER = 47; // 表情
    public static final int TYPE__LOCATION_CARD = 48; // 发送位置
    public static final int TYPE__MSG_CARD = 49; // 消息卡片: 链接、邀请入群、音乐、文件等
    public static final int TYPE__SIGHT = 62; // 小视频
    public static final int TYPE__SYS = 10000; // 系统消息
    public static final int TYPE__SYS_B = 10002; // 系统消息
    public static final int TYPE__ROOM_HINT = 570425393; // 系统消息

}

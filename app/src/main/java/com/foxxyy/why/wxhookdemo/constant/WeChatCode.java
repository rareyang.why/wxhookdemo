package com.foxxyy.why.wxhookdemo.constant;

/**
 * @Author foxxyy
 * @Date 2018-09-29
 */
public class WeChatCode {
    public static final String PACKAGE_NAME = "com.tencent.mm";
    public static final String MAIN_PROCESS_NAME = "com.tencent.mm";
}

package com.foxxyy.why.wxhookdemo.opt;

import android.content.ContentValues;
import android.database.Cursor;
import com.foxxyy.why.wxhookdemo.manager.RobotManager;
import com.foxxyy.why.wxhookdemo.model.Friend;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static de.robv.android.xposed.XposedHelpers.callMethod;

/**
 * @Author foxxyy
 * @Date 2018-10-12
 */
public class WeChatDB {

    private Object SQLDB;

    public WeChatDB(Object dbObject) {
        SQLDB = dbObject;
    }

    // 插入数据
    public void insertSQL(String table, String selection, ContentValues contentValues) {
        callMethod(SQLDB, "insert", table, selection, contentValues);
    }

    // 基础查询
    public Cursor rawQuery(String query, String[] args) {
        return (Cursor) callMethod(SQLDB, "rawQuery", query, args);
    }

    public Cursor rawQuery(String query) {
        return rawQuery(query, null);
    }

    /**
     * 根据微信号、微信ID查询用户
     *
     * @param key 微信号/微信ID
     * @return 微信用户
     */
    public Friend queryWeChatFriend(String key) {
        Cursor cursor = rawQuery(
                "select username, alias, nickname, conRemark from rcontact where alias = ? or username = ?",
                new String[]{key, key}
        );
        if (!validCursor(cursor)) {
            return null;
        }
        String username = cursor.getString(0);
        String alias = cursor.getString(1);
        String nickname = cursor.getString(2);
        String conRemark = cursor.getString(3);
        cursor.close();

        return new Friend(username, alias, nickname, conRemark);
    }

    /**
     * 获得自身的微信信息
     */
    public Friend querySelfWeChatInfo() {
        String sql = "select username, conRemark, nickname, alias from userinfo, rcontact "
                + "where userinfo.id = 2 and rcontact.username = userinfo.value";
        Cursor cursor = rawQuery(sql);
        if (!validCursor(cursor)) {
            return null;
        }

        String username = cursor.getString(cursor.getColumnIndex("username"));
        String conRemark = cursor.getString(cursor.getColumnIndex("conRemark"));
        String nickName = cursor.getString(cursor.getColumnIndex("nickname"));
        String alias = cursor.getString(cursor.getColumnIndex("alias"));
        cursor.close();

        return new Friend(username, alias, nickName, conRemark);
    }

    public String doQuery(String sql) {
        Cursor cursor = rawQuery(sql);
        if (cursor == null || !cursor.moveToFirst()) {
            return "Bad sql or no result.";
        }
        StringBuilder result = new StringBuilder("DoQuery result: \n");
        String[] columnNames = cursor.getColumnNames();
        result.append("COLUMNS: ").append(Arrays.toString(columnNames)).append("\n");
        try {
            if (columnNames != null) {
                do {
                    int length = columnNames.length;
                    for (int i = 0; i < length; i++) {
                        result.append(columnNames[i]).append(": ").append(cursor.getString(i)).append("  ");
                    }
                    result.append("\n");
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            result.append(e.getMessage());
        } finally {
            cursor.close();
        }
        return result.toString();
    }

    /**
     * 查询朋友列表
     */
    public List<String> queryMyFriendList() {
        String sql = "select username from rcontact where username not like '%@chatroom' and type != 0 " +
                " and type &1 != 0 and type != 33 and username != 'weixin' and verifyFlag = 0 " +
                " and username != '" + RobotManager.username() + "' and username not like '%@stranger'";
        Cursor cursor = rawQuery(sql);
        if (!validCursor(cursor)) {
            return null;
        }
        List<String> rst = new ArrayList<>(cursor.getCount());
        do {
            rst.add(cursor.getString(0));
        } while (cursor.moveToNext());

        cursor.close();
        return rst;
    }

    public String[] queryAllMemberInChatroom(String chatroom) {
        String sql = "select memberlist from chatroom where chatroomname = ?";
        String[] args = {chatroom};
        Cursor cursor = rawQuery(sql, args);
        if (cursor != null && cursor.moveToFirst()) {
            String memberList = cursor.getString(cursor.getColumnIndex("memberlist"));
            if (memberList != null) {
                return memberList.split(";");
            }
            cursor.close();
        }
        return null;
    }

    private boolean validCursor(Cursor cursor) {
        if (cursor == null) {
            return false;
        }
        if (!cursor.moveToFirst()) {
            cursor.close();
            return false;
        }
        return true;
    }
}

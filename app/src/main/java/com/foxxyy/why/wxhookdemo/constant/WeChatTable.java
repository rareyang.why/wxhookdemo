package com.foxxyy.why.wxhookdemo.constant;

/**
 * @Author foxxyy
 * @Date 2018-09-30
 */
public class WeChatTable {
    public static final String T_MESSAGE = "message";
    public static final String T_CONTACT = "contact";
    public static final String T_CHATROOM = "chatroom";
    public static final String T_RCONTACT = "rcontact";
}

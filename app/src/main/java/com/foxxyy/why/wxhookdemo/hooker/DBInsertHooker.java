package com.foxxyy.why.wxhookdemo.hooker;

import android.content.ContentValues;
import com.foxxyy.why.wxhookdemo.constant.ChatroomColumn;
import com.foxxyy.why.wxhookdemo.constant.MessageColumn;
import com.foxxyy.why.wxhookdemo.constant.WeChatTable;
import com.foxxyy.why.wxhookdemo.manager.ManualOptManger;
import com.foxxyy.why.wxhookdemo.model.Chatroom;
import com.foxxyy.why.wxhookdemo.model.Message;
import com.foxxyy.why.wxhookdemo.opt.WeChatOpt;
import com.foxxyy.why.wxhookdemo.util.XxLog;
import de.robv.android.xposed.XC_MethodHook;

/**
 * @Author foxxyy
 * @Date 2018-09-30
 */
public class DBInsertHooker extends XC_MethodHook {
    @Override
    protected void afterHookedMethod(MethodHookParam param) throws Throwable {
        if (isMessage(param)) {
            Message message = new Message((ContentValues) param.args[2]);
            if (ManualOptManger.systemInput(message)) { // 手动触发命令
                return;
            }
            XxLog.logParam("xxyy", param);
            String content = message.getContent();
            if (content.contains("xxyy") && message.getIsSend() == 0) {
                WeChatOpt.getInstance().send(message.getTalker(), "Hello world!");
            }
        }
        if (isChatroom(param)) {
            XxLog.logParam("xxyy", param);
            Chatroom chatroom = new Chatroom((ContentValues) param.args[2]);
        }
    }

    private boolean isMessage(XC_MethodHook.MethodHookParam param) {
        return WeChatTable.T_MESSAGE.equals(param.args[0])
                && MessageColumn.MSG_ID.equals(param.args[1])
                && Integer.valueOf(0).equals(param.args[3]);
    }

    private boolean isChatroom(XC_MethodHook.MethodHookParam param) {
        return WeChatTable.T_CHATROOM.equals(param.args[0])
                && ChatroomColumn.CHATROOMNAME.equals(param.args[1])
                && Integer.valueOf(5).equals(param.args[3]);
    }

}

package com.foxxyy.why.wxhookdemo.main;

import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import com.foxxyy.why.wxhookdemo.manager.RobotManager;
import com.foxxyy.why.wxhookdemo.util.XxLog;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedHelpers;

import java.util.Arrays;
import java.util.List;

/**
 * @Author foxxyy
 * @Date 2018-10-18
 */
public class HookTester {
    private HookTester() {
    }

    private static HookTester htInstance = new HookTester();


    public static void hookTester() {
        htInstance.hookDlg();
    }

    private void hookConstructor002() {
        String className = "com.tencent.mm.aw.a";
        XposedHelpers.findAndHookConstructor(
                className,
                RobotManager.loadPackageParam.classLoader,
                String.class,
                int.class,
                int.class,
                new XC_MethodHook() {
                    @Override
                    protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                        XxLog.log("HookC_0202", param);
                    }
                }
        );
    }

    private void hookLog() {
        String[] methods = {"f", "e", "w", "i", "d", "v", "h", "j"};
        for (String method : methods) {
            hookLog(method);
        }
    }

    private void hookLog(String methodName) {
        XposedHelpers.findAndHookMethod(
                "com.tencent.mm.sdk.platformtools.v",
                RobotManager.loadPackageParam.classLoader,
                methodName,
                String.class,
                String.class,
                Object[].class,
                logHooker("log_" + methodName)
        );
    }

    private void hookDlg() {
        for (Object[] objs : dlgParam) {
            hookDlg(objs);
        }
    }

    private void hookDlg(Object[] objs) {
        XposedHelpers.findAndHookMethod(
                "com.tencent.mm.ui.base.h",
                RobotManager.loadPackageParam.classLoader,
                "a",
                objs
        );
    }

    private List<Object[]> dlgParam = Arrays.asList(
            new Object[]{Context.class, String.class, View.class, DialogInterface.OnClickListener.class,
                    logHooker("dlg0")},

            new Object[]{Context.class, String.class, String.class, DialogInterface.OnClickListener.class,
                    logHooker("dlg1")},

            new Object[]{Context.class, int.class, int.class, DialogInterface.OnClickListener.class,
                    logHooker("dlg2")},

            new Object[]{Context.class, String.class, String.class, DialogInterface.OnClickListener.class,
                    DialogInterface.OnClickListener.class, logHooker("dlg3")},

            new Object[]{Context.class, String.class, String.class, String.class, String.class, boolean.class,
                    DialogInterface.OnClickListener.class, DialogInterface.OnClickListener.class,
                    logHooker("dlg4")},

            new Object[]{Context.class, boolean.class, String.class, View.class, String.class, String.class,
                    DialogInterface.OnClickListener.class, DialogInterface.OnClickListener.class, int.class,
                    logHooker("dlg5")},

            new Object[]{Context.class, int.class, String.class, boolean.class, DialogInterface.OnCancelListener.class,
                    logHooker("dlg6")},

            new Object[]{Context.class, String.class, String.class, String.class, String.class, boolean.class,
                    DialogInterface.OnClickListener.class, DialogInterface.OnClickListener.class, int.class,
                    logHooker("dlg7")}
    );


    private XC_MethodHook logHooker(final String note) {
        return new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(XC_MethodHook.MethodHookParam param) throws Throwable {
                Object[] args = param.args;
                XxLog.log("DEBUG", "-------Hooked[" + note + "]----------");
                for (int i = 0; i < args.length; i++) {
                    Object arg = args[i];
                    String result = "[" + note + "] Param arg_" + i + " : ";
                    if (arg == null) {
                        result += " NULL";
                    } else {
                        if (arg instanceof String || arg instanceof Boolean || arg instanceof Number) {
                            result += arg;
                        } else if (arg instanceof Object[]) {
                            result += Arrays.toString((Object[]) arg);
                        } else {
                            Class clazz = arg.getClass();
                            result += clazz.getName() + " - " + clazz.getSimpleName();
                        }
                    }
                    XxLog.log("DEBUG", result);
                }
            }
        };
    }
}

package com.foxxyy.why.wxhookdemo.main;

import android.content.Context;
import com.foxxyy.why.wxhookdemo.constant.WeChatCode;
import com.foxxyy.why.wxhookdemo.manager.RobotManager;
import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

import static de.robv.android.xposed.XposedHelpers.*;

/**
 * @Author foxxyy
 * @Date 2018-09-29
 */
public class XposedInit implements IXposedHookLoadPackage {
    @Override
    public void handleLoadPackage(XC_LoadPackage.LoadPackageParam loadPackageParam) throws Throwable {
        //过滤非微信app
        if (!WeChatCode.PACKAGE_NAME.equals(loadPackageParam.packageName)) {
            return;
        }
        //过滤非微信主进程
        if (!WeChatCode.MAIN_PROCESS_NAME.equals(loadPackageParam.processName)) {
            return;
        }
        //获取系统上下文
        Context sysContext = (Context) callMethod(
                callStaticMethod(findClass("android.app.ActivityThread", null), "currentActivityThread"),
                "getSystemContext"
        );
        XposedBridge.log("start-------------xxyy-----------xxyy--------");
        RobotManager.setLoadPackageParam(loadPackageParam);
        RobotManager.setSysContext(sysContext);
        HookHome.genInstance().hook();
    }
}

package com.foxxyy.why.wxhookdemo.constant;

/**
 * @Author foxxyy
 * @Date 2018-10-18
 */
public class MessageColumn {
    public static final String BIG_IMG_PATH = "bigImgPath";
    public static final String CONTENT = "content";
    public static final String CREATE_TIME = "createTime";
    public static final String IS_SEND = "isSend";
    public static final String IMG_PATH = "imgPath";
    public static final String MSG_ID = "msgId";
    public static final String MSG_SVR_ID = "msgSvrId";
    public static final String STATUS = "status";
    public static final String TYPE = "type";
    public static final String TALKER = "talker";
    public static final String TALKER_ID = "talkerId";
}

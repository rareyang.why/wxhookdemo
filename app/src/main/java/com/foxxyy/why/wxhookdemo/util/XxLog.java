package com.foxxyy.why.wxhookdemo.util;

import android.text.TextUtils;
import com.google.gson.Gson;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;

/**
 * @Author foxxyy
 * @Date 2018-09-30
 */
public class XxLog {
    private static final Gson GSON = new Gson();

    /**
     * 印参数
     */
    public static void logParam(String point, XC_MethodHook.MethodHookParam param) {
        if (param.args == null) {
            return;
        }
        StringBuilder builder = new StringBuilder();
        builder.append('[').append(point).append("]\t");
        for (int i = 0; i < param.args.length; i++) {
            if (i != 0) {
                builder.append('\t');
            }
            builder.append('[').append(i).append(':').append(GSON.toJson(param.args[i])).append(']');
        }
        XposedBridge.log(builder.toString() + "\n");
    }

    public static void info(Object obj) {
        log("INFO", obj);
    }

    public static void error(String desc, Throwable th) {
        String errorStr = "[" + desc + "] " + th + " \n" + StringUtils.join(th.getStackTrace(), "\n");
        log("ERROR", errorStr);
    }

    public static void log(String prefix, Object obj) {
        if (obj instanceof String) {
            logText(prefix, (String) obj);
        } else if (obj instanceof Throwable) {
            logThrowable((Throwable) obj);
        } else {
            logObject(prefix, obj);
        }
    }

    public static void logObject(String prefix, Object obj) {
        if (TextUtils.isEmpty(prefix)) {
            prefix = "";
        }
        XposedBridge.log(prefix + obj.toString());
    }

    public static void logText(String prefix, String text) {
        if (TextUtils.isEmpty(prefix)) {
            prefix = "";
        }
        XposedBridge.log(prefix + " " + text);
    }

    public static void logThrowable(Throwable th) {
        XposedBridge.log(th);
    }
}

package com.foxxyy.why.wxhookdemo.hooker;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.HandlerThread;
import com.foxxyy.why.wxhookdemo.constant.ShSymbol;
import com.foxxyy.why.wxhookdemo.util.ClassUtils;
import com.foxxyy.why.wxhookdemo.util.XxLog;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedHelpers;

public class QrCodeeHooker {

    private static QrCodeeHooker instance = new QrCodeeHooker();

    private QrCodeeHooker() {
        HandlerThread handlerThread = new HandlerThread("choreThread");
        handlerThread.start();
        qrcodeHandler = new Handler(handlerThread.getLooper());
    }


    public static QrCodeeHooker getInstance() {
        return instance;
    }

    private boolean hooked = false;
    private static Handler qrcodeHandler;

    public void hook() {
        if (!hooked) {
            try {
                XposedHelpers.findAndHookMethod(
                        ClassUtils.mmClass(".plugin.setting.ui.setting.SelfQRCodeUI"),
                        "onResume", new QrCodeHookerB());
                hooked = true;
            } catch (Exception e) {
                XxLog.error("In QrCodeeHooker", e);
            }
        }
    }

    private class QrCodeHookerB extends XC_MethodHook {

        @Override
        protected void afterHookedMethod(MethodHookParam param) throws Throwable {
            final Object thisObject = param.thisObject;
            final Intent intent = (Intent) XposedHelpers.callMethod(thisObject, "getIntent");
            if (intent == null || intent.getStringExtra(ShSymbol.SOHU_ACT) == null) {
                return;
            }
            qrcodeHandler.post(new Runnable() {
                @Override
                public void run() {
                    uploadQrcode(thisObject, intent);
                }
            });
        }

        private void uploadQrcode(Object thisObject, Intent intent) {
            try {
                Bitmap bitmap = (Bitmap) XposedHelpers.getObjectField(thisObject, "dSE"); //二维码bitmap
                int cycleTime = 0;
                while (bitmap == null) {
                    bitmap = (Bitmap) XposedHelpers.getObjectField(thisObject, "dSE");
                    Thread.sleep(10L);
                    cycleTime++;
                    if (cycleTime == 150) {
                        break;
                    }
                }
//                final String chatroom = intent.getStringExtra("from_userName");
                if (bitmap == null) {
                    XposedHelpers.callMethod(thisObject, "finish");
                } else {
                    if (bitmap.isRecycled()) {
                        throw new IllegalStateException("Be recycled.");
                    }
                    // TODO: upload to server
                    XposedHelpers.callMethod(thisObject, "finish");
                    if (!bitmap.isRecycled()) {
                        bitmap.recycle();
                    }
                }
            } catch (Throwable throwable) {
                XxLog.error("uploadQrcode", throwable);
            }
        }
    }
}

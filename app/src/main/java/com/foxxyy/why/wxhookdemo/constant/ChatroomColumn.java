package com.foxxyy.why.wxhookdemo.constant;

/**
 * @Author foxxyy
 * @Date 2018-10-18
 */
public class ChatroomColumn {
    public static final String CHATROOMNAME = "chatroomname";
    public static final String ROOM_OWNER = "roomowner";
    public static final String DISPLAYNAME = "displayname";
    public static final String MEMBER_LIST = "memberlist";
    public static final String CHATROOM_NOTICE = "chatroomnotice";
    public static final String SELF_DISPLAY_NAME = "selfDisplayName";
    public static final String MODIFY_TIME = "modifytime";
    public static final String ROOMFLAG = "roomflag";
    public static final String CHATROOM_NOTICE_PUBLISHTIME = "chatroomnoticePublishTime";
    public static final String CHATROOM_NOTICE_NEWVERSION = "chatroomnoticeNewVersion";
    public static final String CHATROOM_DATAFLAG = "chatroomdataflag";
    public static final String CHATROOM_NOTICE_OLDVERSION = "chatroomnoticeOldVersion";
    public static final String STYLE = "style";
    public static final String IS_SHOWNAME = "isShowname";
    public static final String ADDTIME = "addtime";
    public static final String ROW_ID = "rowid";
}

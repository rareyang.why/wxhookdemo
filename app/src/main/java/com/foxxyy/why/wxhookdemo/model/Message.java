package com.foxxyy.why.wxhookdemo.model;

import android.content.ContentValues;
import com.foxxyy.why.wxhookdemo.constant.MessageCValue;

/**
 * @Author foxxyy
 * @Date 2018-09-30
 */
public class Message {
    private Integer msgType;
    private String content;
    private Long msgId;
    private Long msgSvrId;
    private String imgPath;
    private String talker;
    private Integer status;
    private Long createTime; // 秒单位
    private Integer isSend;

    private Boolean isSendByTask;

    public Message() {
    }

    public Message(ContentValues contentValues) {
        this.msgType = contentValues.getAsInteger("type");
        this.content = contentValues.getAsString("content");
        this.msgId = contentValues.getAsLong("msgId");
        this.msgSvrId = contentValues.getAsLong("msgSvrId");
        this.imgPath = contentValues.getAsString("imgPath");
        this.talker = contentValues.getAsString("talker");
        this.status = contentValues.getAsInteger("status");
        this.isSend = contentValues.getAsInteger("isSend");
        Long createTime = contentValues.getAsLong("createTime");
        this.createTime = createTime == null ? null : createTime / 1000;
    }

    public Integer getMsgType() {
        return msgType;
    }

    public void setMsgType(Integer msgType) {
        this.msgType = msgType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getMsgId() {
        return msgId;
    }

    public void setMsgId(Long msgId) {
        this.msgId = msgId;
    }

    public Long getMsgSvrId() {
        return msgSvrId;
    }

    public void setMsgSvrId(Long msgSvrId) {
        this.msgSvrId = msgSvrId;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public String getTalker() {
        return talker;
    }

    public void setTalker(String talker) {
        this.talker = talker;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Integer getIsSend() {
        return isSend;
    }

    public void setIsSend(Integer isSend) {
        this.isSend = isSend;
    }

    public Boolean getSendByTask() {
        return isSendByTask;
    }

    public void setSendByTask(Boolean sendByTask) {
        isSendByTask = sendByTask;
    }

    public boolean isReceivedMsg() {
        return isSend == null
                || MessageCValue.IS_SEND__FALSE == isSend;
    }
}

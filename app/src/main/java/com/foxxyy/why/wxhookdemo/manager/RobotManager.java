package com.foxxyy.why.wxhookdemo.manager;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.foxxyy.why.wxhookdemo.model.Friend;
import com.foxxyy.why.wxhookdemo.opt.WeChatDB;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

/**
 * @Author foxxyy
 * @Date 2018-09-30
 */
public class RobotManager {
    public static XC_LoadPackage.LoadPackageParam loadPackageParam;


    public static WeChatDB weChatDb;
    public static Context sysContext;
    public static Context appContext;
    public static Friend selfWeChat;

    public static Handler weChatMainHandler = new Handler(Looper.getMainLooper());

    public static void setAppContext(Context appContext) {
        RobotManager.appContext = appContext;
    }

    public static void setLoadPackageParam(XC_LoadPackage.LoadPackageParam loadPackageParam) {
        RobotManager.loadPackageParam = loadPackageParam;
    }

    public static void setWeChatDb(WeChatDB weChatDb) {
        RobotManager.weChatDb = weChatDb;
    }

    public static void setSysContext(Context sysContext) {
        RobotManager.sysContext = sysContext;
    }

    public static void setSelfWeChat(Friend selfWeChat) {
        RobotManager.selfWeChat = selfWeChat;
    }

    public static String username() {
        if (selfWeChat == null) {
            if (weChatDb == null) {
                return null;
            }
            selfWeChat = weChatDb.querySelfWeChatInfo();
        }
        return selfWeChat.getUsername();
    }
}
